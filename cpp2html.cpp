/*
 * CSc103 Project 5: Syntax highlighting, part two.
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include "fsm.h"
using namespace cppfsm;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;
#include <map>
using std::map;
#include <initializer_list> // for setting up maps without constructors.

// enumeration for our highlighting tags:
enum {
	hlstatement,  // used for "if,else,for,while" etc...
	hlcomment,    // for comments
	hlstrlit,     // for string literals
	hlpreproc,    // for preprocessor directives (e.g., #include)
	hltype,       // for datatypes and similar (e.g. int, char, double)
	hlnumeric,    // for numeric literals (e.g. 1234)
	hlescseq,     // for escape sequences
	hlerror,      // for parse errors, like a bad numeric or invalid escape
	hlident       // for other identifiers.  Probably won't use this.
};

// usually global variables are a bad thing, but for simplicity,
// we'll make an exception here.
// initialize our map with the keywords from our list:
map<string, short> hlmap = {
#include "res/keywords.txt"
};
// note: the above is not a very standard use of #include...

// map of highlighting spans:
map<int, string> hlspans = {
	{hlstatement, "<span class='statement'>"},
	{hlcomment, "<span class='comment'>"},
	{hlstrlit, "<span class='strlit'>"},
	{hlpreproc, "<span class='preproc'>"},
	{hltype, "<span class='type'>"},
	{hlnumeric, "<span class='numeric'>"},
	{hlescseq, "<span class='escseq'>"},
	{hlerror, "<span class='error'>"}
};
// note: initializing maps as above requires the -std=c++0x compiler flag,
// as well as #include<initializer_list>.  Very convenient though.
// to save some typing, store a variable for the end of these tags:
string spanend = "</span>";

string translateHTMLReserved(char c) {
	switch (c) {
		case '"':
			return "&quot;";
		case '\'':
			return "&apos;";
		case '&':
			return "&amp;";
		case '<':
			return "&lt;";
		case '>':
			return "&gt;";
		case '\t': // make tabs 4 spaces instead.
			return "&nbsp;&nbsp;&nbsp;&nbsp;";
		default:
			char s[2] = {c,0};
			return s;
	}
}
string syntax(string B)	{
	string alllines;
	int state = start;
	for(unsigned int i = 0; i < B.length(); i++)
	{
		string temp= "";
		updateState(state,B[i]);

        switch(state) {
							case start:
								if(B[i] != '"'){
									alllines += translateHTMLReserved(B[i]);
								}
								if(B[i] == '"'){
									temp += translateHTMLReserved(B[i]);
									alllines += hlspans[hlstrlit] + temp + spanend;
								}
								break;
						//case start: We need this one

						case scanid:
                        while(state == scanid){
												temp += B[i];
												i++;
												updateState(state,B[i]);
                        }

                        if (state!= 1){
                                map<string, short>::iterator z;
                                z = hlmap.find(temp);
												if (z != hlmap.end()){
                            alllines += hlspans[hlmap[temp]] + temp + spanend + translateHTMLReserved(B[i]);}
											else {alllines += temp + translateHTMLReserved(B[i]);}}
                        break;

						case comment:
							    while (i - 1 < B.length())  {
										temp += B[i-1];
										i++;
									}
									alllines += hlspans[hlcomment] + temp + spanend;//case comment:We need this one
                    break;

						case strlit:
        temp += B[i];
        alllines += hlspans[hlstrlit] + temp + spanend;

				break;   //case strlit:We need this one


						case readfs:
								if (state != comment){
										temp += B[i];
									 }
									 break;//case readfs:We need this one


						case readesc:
									temp += translateHTMLReserved(B[i]);
									i++;
									if(B[i]== 'n') {
									temp += B[i];
									alllines += hlspans[hlescseq] + temp + spanend;
												temp.clear();
												state = strlit;}

									else{temp += B[i];
								alllines += hlspans[hlerror]+ temp + spanend;
									}
                        break;

                  case scannum:
                   while (state == scannum){
												  temp += B[i];
										      i++;
										      updateState(state,B[i]);
									       }
												  i--;
												  alllines += hlspans[hlnumeric] + temp + spanend;
													break;												 //case scannum: We also need this one

                case error:
										temp += B[i];
										alllines += hlspans[hlerror] + temp + spanend;
                        break;
        }

}

        return alllines;
}

int main() {
	// TODO: write the main program.
	// It may be helpful to break this down and write
	// a function that processes a single line, which
	// you repeatedly call from main().

		string s;
	while(getline(cin, s))
	{
        cout<<syntax(s)<< endl;
	}
	return 0;
}
